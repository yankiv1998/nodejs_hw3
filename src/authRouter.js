const express = require('express');
// eslint-disable-next-line new-cap
const authRouter = express.Router();
const {User} = require('./models/userModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const generateAccessToken = (id, email, role) => {
  const payload ={
    id,
    email,
    role,
  };
  return jwt.sign(payload, process.env.SECRET_TOKEN, {expiresIn: '2h'});
};

authRouter.post('/register', async (req, res)=> {
  try {
    const {email, password, role} = req.body;
    const candidate = await User.findOne({email});
    if (candidate) {
      throw new Error('User already exist');
    }
    const hashPassword = bcrypt.hashSync(password, 6);
    const user = new User(
        {
          role,
          email,
          password: hashPassword,
          created_date: new Date().toISOString(),
        },
    );
    await user.save();
    return res.json({message: 'Profile created successfully'});
  } catch (e) {
    console.log(e);
    res.status(400).json({message: e.message});
  }
});

authRouter.post('/login', async (req, res)=>{
  try {
    const {email, password} = req.body;
    const user = await User.findOne({email});
    if (!user) {
      return res.status(400).json({message: `User not found`});
    }
    const validPassword = bcrypt.compareSync(password, user.password);
    if (!validPassword) {
      return res.status(400).json({message: 'Password is not valid'});
    }
    const token = generateAccessToken(user._id, user.email, user.role);
    return res.json({jwt_token: token, message: 'Success'});
  } catch (e) {
    console.log(e);
    res.status(400).json({message: 'Login error'});
  }
});


module.exports = {
  authRouter,
};
