const {Schema, model} = require('mongoose');

const dimensionsSchema = new Schema({
  width: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
  length: {
    type: Number,
    required: true,
  },
});
const logSchema = new Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  },
});

const loadSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  status: {
    type: String,
    required: true,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    enum: [
      'En route to Pick Up',
      'Arrived to pick up', 'En route to delivery', 'Arrived to delivery'],
  },
  created_date: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: dimensionsSchema,
    required: true,
  },
  logs: {
    type: [logSchema],
    required: true,
  },
});

const Load = model('loads', loadSchema);

module.exports = {
  Load,
};
