/* eslint-disable linebreak-style */
const {Schema, model} = require('mongoose');

const truckSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    required: true,
    enum: ['OL', 'IS'],
  },
  created_date: {
    type: String,
    required: true,
  },
});

const Truck = model('truck', truckSchema);

module.exports = {
  Truck,
};
