const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  created_date: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
  },
});

const User = model('users', userSchema);

module.exports = {
  User,
};
