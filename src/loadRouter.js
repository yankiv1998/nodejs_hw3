/* eslint-disable require-jsdoc */
/* eslint-disable camelcase */
/* eslint-disable linebreak-style */
const express = require('express');
// eslint-disable-next-line new-cap
const loadRouter = express.Router();
const {Load} = require('./models/loadModel');
const authMiddleware = require('./authMiddleware');


loadRouter.get('', authMiddleware.authMiddleware, async (req, res) =>{
  try {

  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});
/* {
  "name": "Moving sofa",
  "payload": 100,
  "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
  "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
  "dimensions": {
    "width": 44,
    "length": 32,
    "height": 66
  }
} */
loadRouter.post('', authMiddleware.authMiddleware, async (req, res) =>{
  try {
    const {
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;

    const load = new Load({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      created_date: new Date().toISOString(),
      created_by: req.user._id,
      status: 'NEW',
      logs: [{
        time: new Date().toISOString(),
        message: 'Load has been created',
      }],
    });
    await load.save();
    return res.json({message: 'Load created successfully'});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});

loadRouter.get('/active', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const load = await Load.findOne({assigned_to: req.user._id});
    return res.json({load});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});
loadRouter.patch(
    '/active/state',
    authMiddleware.authMiddleware,
    async (req, res) => {
      try {
        const load = await Load.findOne({assigned_to: req.user._id});
        const newState = getNextState(load.state);
        load.state = newState;
        await load.save();
        return res.json({'message': `Load state changed to '${newState}'`});
      } catch (err) {
        console.log(err);
        res.status(400).json({message: 'Error'});
      }
    });

loadRouter.get('/:id', authMiddleware.authMiddleware, async (req, res) => {
  try {
    let load = await Load.findById(req.params.id);
    if (load?.created_by !== req.user._id) {
      load = null;
    }
    return res.json({load});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});
loadRouter.put('/:id', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    if (load?.created_by !== req.user._id) {
      throw new Error();
    }
    const {
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    load.name = name;
    load.payload = payload;
    load.pickup_address = pickup_address;
    load.delivery_address = delivery_address;
    load.dimensions= dimensions;
    await load.save();
    return res.json({'message': 'Load details changed successfully'});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});

loadRouter.delete('/:id', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const load = await Load.findById(req.params.id);
    if (load?.created_by !== req.user._id) {
      throw new Error();
    }
    await load.deleteOne();
    return res.json({'message': 'Load deleted successfully'});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});

loadRouter.post(
    '/:id/post',
    authMiddleware.authMiddleware,
    async (req, res) => {
      try {
        const load = await Load.findById(req.params.id);
        if (load?.created_by !== req.user._id) {
          throw new Error();
        }

        return res.json({
          'message': 'Load posted successfully',
          'driver_found': true,
        });
      } catch (err) {
        console.log(err);
        res.status(400).json({message: 'Error'});
      }
    });

loadRouter.get(
    '/:id/shipping_info',
    authMiddleware.authMiddleware,
    async (req, res) => {
      try {
        const load = await Load.findById(req.params.id);
        if (load?.created_by !== req.user._id) {
          throw new Error();
        }

        return res.json({load});
      } catch (err) {
        console.log(err);
        res.status(400).json({message: 'Error'});
      }
    });

function getNextState(state) {
  if (state === 'En route to Pick Up') {
    return 'Arrived to pick up';
  } else if (state === 'Arrived to pick up') {
    return 'En route to delivery';
  } else if (state === 'En route to delivery') {
    return 'Arrived to delivery';
  }
  throw new Error('Next state do not exist');
}

module.exports = {
  loadRouter,
};

