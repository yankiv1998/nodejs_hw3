/* eslint-disable linebreak-style */
const express = require('express');
// eslint-disable-next-line new-cap
const truckRouter = express.Router();
const {Truck} = require('./models/truckModel');
const authMiddleware = require('./authMiddleware');

truckRouter.post('', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const {type} = req.body;
    const truck = new Truck({
      type,
      created_date: new Date().toISOString(),
      created_by: req.user._id,
      status: 'IS',
    });
    await truck.save();
    return res.json({message: 'Truck created successfully'});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Truck creation error'});
  }
});
truckRouter.get('', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const trucks = await Truck.find({
      created_by: req.user._id,
    });

    return res.json({trucks});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});
truckRouter.get('/:id', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const id = req.params.id;
    let truck = await Truck.findById(id);

    if (truck?.created_by !== req.user._id) {
      truck = null;
    }

    return res.json({truck});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});
truckRouter.put('/:id', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const id = req.params.id;
    const truck = await Truck.findById(id);

    if (truck?.created_by !== req.user._id) {
      throw new Error();
    }

    truck.type = req.body.type;
    await truck.save();
    return res.json({'message': 'Truck details changed successfully'});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});
truckRouter.delete('/:id', authMiddleware.authMiddleware, async (req, res) => {
  try {
    const id = req.params.id;
    const truck = await Truck.findById(id);

    if (truck?.created_by !== req.user._id) {
      throw new Error();
    }

    await truck.deleteOne();

    return res.json({'message': 'Truck deleted successfully'});
  } catch (err) {
    console.log(err);
    res.status(400).json({message: 'Error'});
  }
});

truckRouter.post(
    '/:id/assign',
    authMiddleware.authMiddleware,
    async (req, res) => {
      try {
        const id = req.params.id;
        const truck = await Truck.findById(id);

        if (truck?.created_by !== req.user._id) {
          throw new Error();
        }

        truck.assigned_to = req.user._id;
        await truck.save();

        return res.json({'message': 'Truck assigned successfully'});
      } catch (err) {
        console.log(err);
        res.status(400).json({message: 'Error'});
      }
    });

module.exports = {
  truckRouter,
};
