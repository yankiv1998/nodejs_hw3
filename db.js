const mongoose = require('mongoose');
module.exports = {
  dbconnect: async ()=>{
    await mongoose.connect(process.env.MONGODB_URL);
    await mongoose.connection.dropDatabase();
  },
};
